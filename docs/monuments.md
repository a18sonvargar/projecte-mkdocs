# Top 10 Monuments of Barcelona

### 1. La Sagrada Familia: 
this cathedral designed by the great architect Antoni Gaudí is the emblem of Barcelona, in every souvenir of the city will appear this monument and you HAVE to visit it. To get into the Sagrada Familia is a bit expensive and there are always queues out the door, so take it easy.


### 2. La Pedrera: 
also known as Casa Milà is one of the most amazing buildings of Paseo de Gracia with its own sinuous and organic shapes of Art Nouveau. If you want to go inside you can visit a house simulation of the time and free exhibitions on the first floor. The views from the terrace are also impressive…


### 3. Parc Güell: 
this park is in the upper area of the city coming from the district of Gracia and is also a must if you want to know the Gaudi architecture of this Catalan city. Opening hours are from 8am to 6pm in autumn and winter and from 8 am to 21.30 in summer and spring, and the entrance fee is 7,50€. Again, kind of expensive but it will be worth paying.


### 4. Casa Batlló: 
you will find it also going down the Paseo de Gracia on your right, and as La Pedrera is another representation of modernist architecture and decoration. In this case the price of the general admission is totally unfair, and as a local inhabitant I admit it, but considering it is one of the greatest exponents of the artistic style maybe you’re willing to pay to enter.


### 5. La Catedral de Barcelona: 
Recently renovated, this Gothic cathedral will take off your breath the moment you get into the square where is located until you get out of it. Meet the liturgical history of the city through this gigantic church in the middle of Barcelona.


### 6. The “Boqueria” market: 
down Las Ramblas on the near the beginning, you will find this iconic market town that shouldn’t be missed out on. The fresh food stalls are divided according to the food they sell and you will not find fresher and cheaper food in the center of Barcelona than in this market.


### 7. La Statue of Colón: 
at the very end of the Ramblas, across a pedestrian crossing you will find the statue of Cristobal Colón pointing towards America. At “his” feet is common to see tourists taking pictures with huge stone lions that are guarding the monument, just before going up the statue with a lift and enjoying excellent views of Barcelona.


### 8. La Church of Santa Maria del Mar: 
one of the most beautiful Gothic churches in Barcelona (and probably in Spain) located in the Borne neighborhood. Inspired Ildefonso Falcones to write “La Catedral del Mar“.


### 9. El Palau de la Música Catalana: 
This architectural work of Domenech and Montaner is one of the best concert halls in Barcelona with an excellent acoustic and an also is an essential representation of Catalan Modernism.


10. The Montjuïc Castle: and last but not least, on top of the mountain of Montjuïc there is the castle with the same name, a former military fortress to which you can climb on foot or by cable car and is packed with guns and other military devices so you can take some funny pictures there. Also enjoy excellent views of the city while you climb.