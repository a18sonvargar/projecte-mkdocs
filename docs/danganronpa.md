# Best Characters

#### 1. Sonia Nevermind

![N](https://vignette.wikia.nocookie.net/danganronpa/images/f/f1/SoniaNevermindDR2.png/revision/latest/scale-to-width-down/350?cb=20170422052247)

Sonia Nevermind (ソニア・ネヴァーマインド), is a student of Hope's Peak Academy's Class 77-B, and a participant of the Killing School Trip featured in Danganronpa 2: Goodbye Despair. Her title is the Ultimate Princess (超高校級の「王女」 lit. Super High School Level Princess).

She, along with Class 77-B, return in Danganronpa 3: The End of Hope's Peak High School to explore their school life leading up to The Tragedy.


#### 2. Gundham Tanaka

![N](https://www.pngkit.com/png/full/404-4046334_evil-anime-boy-with-black-hair-and-red.png)

Gundham Tanaka (田中 眼蛇夢), is a student of Hope's Peak Academy's Class 77-B, and a participant of the Killing School Trip featured in Danganronpa 2: Goodbye Despair. His title is the Ultimate Breeder (超高校級の「飼育委員」 lit. Super High School Level Animal Breeder).

He, along with Class 77-B, return in Danganronpa 3: The End of Hope's Peak High School to explore their school life leading up to The Tragedy. 


#### 3. Nagito Komaeda

![N](https://vignette.wikia.nocookie.net/danganronpa/images/e/e2/NagitoKomaedaDR2.png/revision/latest/scale-to-width-down/350?cb=20170421142208)

Nagito Komaeda (狛枝 凪斗), is a student of Hope's Peak Academy's Class 77-B, and a participant of the Killing School Trip featured in Danganronpa 2: Goodbye Despair. His title is the Ultimate Lucky Student (超高校級の「幸運」 lit. Super High School Level Good Luck).

He, along with Class 77-B, return in Danganronpa 3: The End of Hope's Peak High School to explore their school life leading up to The Tragedy.

He also makes an appearance in Danganronpa Another Episode: Ultra Despair Girls as "Servant" (召使い meshitsukai). He returns once again as the protagonist of the OVA Super Danganronpa 2.5: Komaeda Nagito to Sekai no Hakaimono, exploring his ideal world.


#### 4. Junko Enoshima

![N](https://vignette.wikia.nocookie.net/danganronpa/images/5/59/Danganronpa_The_Animation_Junko_Enoshima_Sidebar.png/revision/latest/scale-to-width-down/350?cb=20170529232810)

Junko Enoshima (江ノ島 盾子), is a student in Hope's Peak Academy's Class 78th, and a participant of the Killing School Life featured in Danganronpa: Trigger Happy Havoc. Her title is the Ultimate Fashionista (超高校級の「ギャル」 lit. Super High School Level Fashion Girl). 


#### 5. Kyoko Kirigiri

![N](https://vignette.wikia.nocookie.net/danganronpa/images/3/39/Danganronpa_The_Animation_Kyoko_Kirigiri.png/revision/latest/scale-to-width-down/350?cb=20170721062217)

Kyoko Kirigiri (霧切 響子), is a student in Hope's Peak Academy's Class 78th, and a participant of the Killing School Life featured in Danganronpa: Trigger Happy Havoc. She doesn't remember her talent at the beginning of the Killing Game, so her title is the Ultimate ??? (超高校級の「???」 lit. Super High School Level ???).

Years later, she is a participant of the Final Killing Game featured in Danganronpa 3: The End of Hope's Peak High School with the other Future Foundation Directors.

Her past as a young teenager is explored in the novel series Danganronpa Kirigiri. 


#### 6. Celestia Ludenberg

![N](https://vignette.wikia.nocookie.net/danganronpa/images/3/36/Danganronpa_1_Celestia_Ludenberg_Sprite_Sidebar.png/revision/latest/scale-to-width-down/350?cb=20170601130454)

Celestia Ludenberg (セレスティア・ルーデンベルク), also known as Celeste, is a student in Hope's Peak Academy's Class 78th, and a participant of the Killing School Life featured in Danganronpa: Trigger Happy Havoc. Her title is the Ultimate Gambler (超高校級の「ギャンブラー」 lit. Super High School Level Gambler). 


