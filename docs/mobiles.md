# Top 8 Smartphones 2019


| Smartphone | Release Date |
| ------ | ------ |
| Samsung Galaxy S10 Plus | 8 March 2019 |
| iPhone 11 | September 20, 2019	 |
| Samsung Galaxy Note 10 Plus |  ‎August 7, 2019 |
| iPhone 11 Pro Max | ‎September 20, 2019 |
| Samsung Galaxy S10e | ‎8 March 2019 |
| OnePlus 7 Pro | May 14, 2019 |
| Google Pixel 4 XL | October 24, 2019 |
| Huawei P30 Pro | March 26, 2019 |
